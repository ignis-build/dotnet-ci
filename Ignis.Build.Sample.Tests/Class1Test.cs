namespace Ignis.Build.Sample;

public class Class1Test
{
    [Fact]
    public void Test1()
    {
        var actual = (object?)new Class1();

        PAssert.IsTrue(() => actual != null);
    }
}
