#!/usr/bin/env bash

set -e
SCRIPT_DIR=$(cd -- "$(dirname "$0")" && pwd)

set -ex

dotnet tool install --tool-path "$SCRIPT_DIR" Ignis.ReSharper.Reporter.Tool

"$SCRIPT_DIR/resharper-reporter" "$@"
