#!/usr/bin/env bash

set -e
SCRIPT_DIR=$(cd -- "$(dirname "$0")" && pwd)

set -ex

dotnet tool install --tool-path "$SCRIPT_DIR" JetBrains.ReSharper.GlobalTools

"$SCRIPT_DIR/jb" inspectcode "$@"
