#!/usr/bin/env bash

set -e
SCRIPT_DIR=$(cd -- "$(dirname "$0")" && pwd)

set -ex

dotnet tool install --tool-path "$SCRIPT_DIR" GitVersion.Tool

"$SCRIPT_DIR/dotnet-gitversion" /config templates/dotnet/GitVersion.yml "$@"
